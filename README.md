# [kurtka.info](https://kurtka.info) source codes

<br/>

### Run kurtka.info on localhost

    # vi /etc/systemd/system/kurtka.info.service

Insert code from kurtka.info.service

    # systemctl enable kurtka.info.service
    # systemctl start kurtka.info.service
    # systemctl status kurtka.info.service

http://localhost:4057
